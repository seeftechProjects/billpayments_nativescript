import { Component, OnInit } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { DataService, IDataItem } from "../core/data.service";
import { ObservableArray, ChangedData } from "tns-core-modules/data/observable-array";


@Component({
    selector: "Home",
    moduleId: module.id,
    templateUrl: "./home.component.html",
    styles:[`
    .gridClass{
    margin:20px;
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
    }
    .stackClass{
        margin-left:30px;
        border:1px solid black;
    }   
    .gridHeaderClass{
        background-color:skyblue;
        height:30;
        padding-top:5px;
        padding-right:20px;
        text-align:center;
    }
    .header{
        margin-right:7px;
    }
    
    `]
})
export class HomeComponent implements OnInit {
    items: ObservableArray<IDataItem>;
   

    constructor(private itemService: DataService, private router: RouterExtensions) { }

    ngOnInit(): void {
        this.items = new ObservableArray(this.itemService.getItems());
    }
   
      

    onItemSelected(event:any){
        console.log("onItemSelected",event.index);
        console.log("onItemSelected",event.object);

    }

    onItemDeselected(event:any){
        console.log("onItemDeselected",event);
    }
}

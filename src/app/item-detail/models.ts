import { ValueList } from "nativescript-drop-down";
export class BackedResponse {
    success: boolean;
    message: string;
}

export class EntityResponse<T> extends BackedResponse {
    body: T;
}

export class Response<T> {
    success: boolean;
    message: string;
    body: T;
}

export class PaymentCategoryDTO {
    valueList:ValueList<PaymentCategoryDTO>;
    name: string;
    id: number;
    uid: string;
    parentId: number;
    children: PaymentCategoryDTO[];
    paymentConfig: PaymentConfigDTO;
    orgUid: string;
}

export class PaymentConfigDTO {
    id: number;
    uid: string;
    name : string;
    amount: number;
    chargesIncluded: boolean;
    dataConfigsDTOs: PaymentDataConfigDTO[];
    parent : PaymentConfigDTO;

}

export class PaymentDataConfigDTO {
    id: number;
    uid: string;
    name: string;
    mandatory: string;
    data: string;
}
export class PaymentPageConfigurationDTO {
    organizationName : string;
    aboutUs : string;
    contactUs : string;
    orgLogoUid : string;
}

export class CommonPaymentConfigDTO {
    paymentCategory : PaymentCategoryDTO;
    paymentPageConfigurationDTO : PaymentPageConfigurationDTO;
}

export class CollectPaymentRequest{
    productUid:string;
    amount:number;
    additionalInfo:any;
    currency : string;
    appCode : string;
    description : string;
}

export class PaymentDataDTO {
    params: any;
}

export class PaymentDetails{
    status: string;
    uid:string;
    description : string;
    currencyCode : string;
    error : string;
    paymentRefNumber : string;
    paymentMethod:string;
    amount:number;
    additionalInfo : AdditionalPaymentInfo[];
    completionUpdatedOn : Date;
}

export class AdditionalPaymentInfo{
    name : string;
    value : string;
    uid : string;
}
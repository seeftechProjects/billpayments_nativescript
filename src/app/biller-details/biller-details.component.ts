import { Component,OnInit } from "@angular/core";
import { BillerDetailsService } from "./biller-details.service";
import { BillDto, Organization } from "~/app/common/models/models";
import { ActivatedRoute } from "@angular/router";
import { RouterExtensions } from "nativescript-angular/router";


@Component({
    selector:"biller-details",
    moduleId: module.id,
    templateUrl:"./biller-details.component.html",
    styles:[`
    .gridClass{
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
    margin-right:25px;
    }
    .stackClass{
        margin-left:30px;
        border:1px solid black;
    }   
    .gridHeaderClass{
        background-color:skyblue;
        height:35;
        padding-top:7px;
       
        
    }
    .header{
        margin-right:7px;
    }
   
    
    `]

})
export class BillerDetailsComponent implements OnInit{
    billerDto:BillDto;
    organization:Organization;
    billerDetailsList:BillDto[];
    orgUid:string;
    customerId:string;
    customerName:string;
    phoneNo:string;
    email:string
    tradingName:string;
    onCheck:boolean;
    onSearch:boolean=false;
    noRecords:boolean=false;
    totalBillAmount:number=0;
    showTotalBillAmount:boolean=false;
    status:string;
    selectedBills:BillDto[];

constructor(private routerExtensions: RouterExtensions,private billerService:BillerDetailsService,private activatedRoute:ActivatedRoute ){}
    ngOnInit(): void{
         this.orgUid = this.activatedRoute.snapshot.params.uid;
        this.tradingName=this.activatedRoute.snapshot.params.tradingName;
        this.selectedBills=new Array<BillDto>();
        
    }

    onSearchBill(event:any){
        console.log("organization",this.orgUid);
        console.log("organization",this.customerId);
     this.billerService.getAllBillerDetail(this.orgUid,this.customerId).subscribe((data)=>{
         this.billerDetailsList=data.body;
         if(data.body.length!=0){
         console.log("BillerList:",data.body)
         this.customerName=this.billerDetailsList[0].customerName;
         console.log("customerName:",this.customerName)
         this.phoneNo=this.billerDetailsList[0].mobile;
         this.email=this.billerDetailsList[0].customerEmail;
         this.onSearch=true;
         this.noRecords=false;
         }else{
            this.onSearch=false;
           this.noRecords=true;
           
         }
     });
    }
    onItemSelected(dto:BillDto){
       if(this.selectedBills.indexOf(dto)==-1){
        this.selectedBills.push(dto);
       }
        
        this.calculateAmmount();
    }
    onItemDeselected(dto:BillDto){
        if(this.selectedBills.indexOf(dto)!=-1){
            this.selectedBills.splice(this.selectedBills.indexOf(dto),1);
           }
        this.calculateAmmount();
    }

    calculateAmmount(){
        this.totalBillAmount=0;
        this.selectedBills.forEach(e=>{
            this.totalBillAmount=this.totalBillAmount+e.amount;
        })

        if(this.totalBillAmount!=0){
            this.showTotalBillAmount=true;
        }else{
            this.showTotalBillAmount=false
        }
    }


    public goBack() {
        this.routerExtensions.backToPreviousPage();
    }

    checkedChange(checkedChange){
        console.log("test checkedChange",checkedChange);
    }
    billChecked:any;
    public onBillTapped(args) {
		console.log("args : ", args);
			const checkbox = args.object;
			const bill:BillDto = args.object.get("bill");
            const serviceId = args.object.get("serviceId");
            const checked =args.object.get("checked");
            console.log("bill : ", bill);
            console.log("serviceId : ", serviceId);
            console.log("billChecked : ", checked);
            if(checked){
                this.onItemSelected(bill);
            } else if(!checked){
                this.onItemDeselected(bill);
            }
           
			
	}
}

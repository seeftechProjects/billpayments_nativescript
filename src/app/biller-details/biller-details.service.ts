import { Injectable } from '@angular/core';
import { Http,Headers,RequestOptions,Response} from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators'
import { HttpClient, HttpHeaders, HttpResponse } from "@angular/common/http";
import {EntityResponse,PagedBackedResponse, Organization,BillDto,BackedResponse } from '../common/models/models';
import { AppConstants } from '../app-constants.service';

@Injectable()
export class BillerDetailsService{
    constructor(private http:Http, private appConst : AppConstants){}

    getAllBillerDetail(orgUid:string,customerId:string):Observable<EntityResponse<BillDto[]>>{
        return this.http.get(this.appConst.API_BASE_URL+"bp/public/bills/findByOrgUidAndCustomerId"+"/"+orgUid+"/"+customerId).pipe(map((response:Response)=>{
            return<EntityResponse<BillDto[]>>response.json()
        }))
    }

    

}

import { NgModule, NgModuleFactoryLoader, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { AppRoutingModule, COMPONENTS } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { CoreModule } from "./core/core.module";
import { PaymentTypeService } from "./payment/services/payment.service";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { HttpModule } from '@angular/http';
import { NativeScriptHttpModule } from "nativescript-angular/http";
import { DropDownModule } from "nativescript-drop-down/angular";
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";
import { TNSCheckBoxModule } from 'nativescript-checkbox/angular';
import {BrowserService } from "./browse/services/browser.service";
import {OrganizationSearchService}  from "./payment-search/services/organization-search.service";
import { BillerDetailsService } from "~/app/biller-details/biller-details.service";
import { BarcodeScanner } from 'nativescript-barcodescanner';
import { registerElement } from "nativescript-angular/element-registry";
import {CardView} from 'nativescript-cardview';
import { AppConstants } from "./app-constants.service";
import { NativeScriptUISideDrawerModule } from "nativescript-ui-sidedrawer/angular";
 registerElement('CardView', () => CardView);

@NgModule({
    bootstrap: [
        AppComponent,
        
    ],
    imports: [
        TNSCheckBoxModule,
        NativeScriptModule,
        AppRoutingModule,
        CoreModule,
        NativeScriptCommonModule,
        NativeScriptFormsModule,
        HttpModule,
        NativeScriptHttpModule,
        DropDownModule,
        NativeScriptUIListViewModule,
        NativeScriptUISideDrawerModule
        
    ],
    declarations: [
        AppComponent,
        COMPONENTS,
    ],
    providers:[
        PaymentTypeService,
        BrowserService,
        OrganizationSearchService,
        BillerDetailsService,
        BarcodeScanner,
        AppConstants
   ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class AppModule { }

import { ValueList } from "nativescript-drop-down";
export class BackedResponse {
    success: boolean;
    message: string;
}

export class EntityResponse<T> extends BackedResponse {
    body: T;
}

export class Response<T> {
    success: boolean;
    message: string;
    body: T;
}

export class PaymentCategoryDTO {
    valueList:ValueList<PaymentCategoryDTO>;
    name: string;
    id: number;
    uid: string;
    parentId: number;
    children: PaymentCategoryDTO[];
    paymentConfig: PaymentConfigDTO;
    orgUid: string;
}

export class PaymentConfigDTO {
    id: number;
    uid: string;
    name : string;
    amount: number;
    chargesIncluded: boolean;
    dataConfigsDTOs: PaymentDataConfigDTO[];
    parent : PaymentConfigDTO;

}

export class PaymentDataConfigDTO {
    id: number;
    uid: string;
    name: string;
    mandatory: string;
    data: string;
}
export class PaymentPageConfigurationDTO {
    organizationName : string;
    aboutUs : string;
    contactUs : string;
    orgLogoUid : string;
}

export class CommonPaymentConfigDTO {
    paymentCategory : PaymentCategoryDTO;
    paymentPageConfigurationDTO : PaymentPageConfigurationDTO;
}

export class CollectPaymentRequest{
    productUid:string;
    amount:number;
    additionalInfo:any;
    currency : string;
    appCode : string;
    description : string;
}

export class PaymentDataDTO {
    params: any;
}

export class PaymentDetails{
    status: string;
    uid:string;
    description : string;
    currencyCode : string;
    error : string;
    paymentRefNumber : string;
    paymentMethod:string;
    amount:number;
    additionalInfo : AdditionalPaymentInfo[];
    completionUpdatedOn : Date;
}

export class AdditionalPaymentInfo{
    name : string;
    value : string;
    uid : string;
}
export class Organization{
    id: number;
    uID: string;
    name: string;
    tradingName: string;
    orgLogoUid: string;
    communicationEmail:string;
    service:ServiceDto[];
}
export class PaymentsDTO {
      id:number;
      uid:string;
      orgUid:string;
      amount:number;
      paymentRefNum:string;
      createdOn:Date;
}

export class PagedBackedResponse<T> extends BackedResponse {
    pageNumber: number;
    totalItems: number;
    totalPages: number;
    pageSize: number;
    numberOfElements: number;
    totalElements: number;
    items: T[];
    body: T[];
}
export class BillDto  {
    id:number;
    uid:string;
    orgUid:string;
    customerName:string;
    customerEmail:string;
    mobile:string;
    amount:number;
    description:string;
    customerId:string;
    daysBeforeToStopAcceptPayments:number;
    dueDate:Date;
    status:string;
    importedDate:string;
    userId:string;
    checked:boolean;
}
export class ServiceDto {
    id:number;
    uid:string;
    name:string;
    code:string;
  }
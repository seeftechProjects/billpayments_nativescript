import { PaymentAmountComponent } from './browse/payment-amount.component';
import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { PostLoginLayoutComponent } from "./layout/post-login-layout";
import { HomeComponent } from "./home/home.component";
import { BrowseComponent } from "./browse/home.component";
import { SearchComponent } from "./search/search.component";
import { ItemDetailComponent } from "./item-detail/item-detail.component";
import { PaymentComponent } from "./payment/components/payment.component";
import { OrganizationSearchComponent } from './payment-search/component/organization-search.component';
import { BillerDetailsComponent } from "./biller-details/biller-details.component";
export const COMPONENTS = [
    BrowseComponent, 
    HomeComponent, 
    ItemDetailComponent, 
    SearchComponent,
    PaymentComponent,
    OrganizationSearchComponent,
    BillerDetailsComponent,
    PaymentAmountComponent,
  
    
];
const routes: Routes = [
   
            { path: "", redirectTo: "/(homeTab:home//browseTab:browse//billerTab:biller//paymentTab:organization)", pathMatch: "full" },
            
                { path: "home", component: HomeComponent, outlet: "homeTab" },
                { path: "browse", component: BrowseComponent, outlet: "browseTab" },
                { path: "payment/:uid/:tradingName", component: PaymentComponent, outlet: "browseTab" },
                {path:"paymentAmount/:uid/:tradingName", component:PaymentAmountComponent,outlet:"browseTab"},
                {path:"organization", component:OrganizationSearchComponent,outlet:"paymentTab"},
                { path: "payment/:uid/:tradingName", component: PaymentComponent, outlet: "paymentTab" },
                {path:"billerdetails/:uid/:tradingName", component:BillerDetailsComponent, outlet: "searchTab"},
                { path: "biller", component: SearchComponent, outlet: "billerTab" },
                
                
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }

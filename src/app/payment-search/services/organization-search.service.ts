import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators'
import { HttpClient, HttpHeaders, HttpResponse } from "@angular/common/http";
import { Organization, EntityResponse } from '~/app/common/models/models';
import { AppConstants } from '~/app/app-constants.service';

@Injectable()
export class OrganizationSearchService {
    constructor(private http: Http, private appConst : AppConstants) { }

    getAllOrganizationList(name: string): Observable<EntityResponse<Organization[]>> {
        return this.http.get(this.appConst.API_BASE_URL+"oauth/public/organization/findByName" + "/" + name).pipe(map((response: Response) => {
            return <EntityResponse<Organization[]>>response.json();
        }))
    }
    findOrginazationByMobileNumber(mobileNumber: string): Observable<EntityResponse<Organization>> {
        return this.http.post(this.appConst.API_BASE_URL+"oauth/public/organization/findByMobileNumber", mobileNumber).pipe(map((response :Response)=>{
            return <EntityResponse<Organization>> response.json();
    }))
    }

    findOrginazationByCode(orgCode: string): Observable<EntityResponse<Organization>> {
        return this.http.post(this.appConst.API_BASE_URL+"oauth/public/organization/findByOrgCode", orgCode).pipe(map((response :Response)=>{
            return <EntityResponse<Organization>> response.json();
    }))
    }

}
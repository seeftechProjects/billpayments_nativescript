import { Component } from '@angular/core';
import {Organization} from '../../common/models/models';
import { OrganizationSearchService } from '~/app/payment-search/services/organization-search.service';
import { TextField } from "tns-core-modules/ui/text-field";
import { Router,ActivatedRoute  } from '@angular/router';
import { BarcodeScanner } from 'nativescript-barcodescanner';
import * as dialogs from "tns-core-modules/ui/dialogs";
@Component({
    selector:"organization-search",
    moduleId: module.id,
    templateUrl: "./organization-search.component.html",
   
})
export class OrganizationSearchComponent{
    organizations:Organization[];
    organization:Organization;
    name:string;


   constructor(private barcodeScanner:BarcodeScanner, private organizationSearchService:OrganizationSearchService,private router:Router){}

    ngOnInit(){
    }

    onTestChange(event:any){
    
      let textField = <TextField>event.object;
              console.log("onTextChange");
              this.name = textField.text;
              console.log("name",this.name);
        this.organizationSearchService.getAllOrganizationList(this.name).subscribe((data)=>{
            this.organizations=data.body;
            console.log("organizations:",data.body);
        })
    }
    onItemTap(event:any){
        console.log("clicked");
    }


    onScan() {
       
        console.log("test route");
    this.barcodeScanner.scan({ formats: "QR_CODE, EAN_13",
                    showFlipCameraButton : true,
                    preferFrontCamera: false,
                    showTorchButton : true,
                    beepOnScan: true,
                    torchOn: false,
                    resultDisplayDuration: 500,
                    openSettingsIfPermissionWasPreviouslyDenied: true }).then((result) => {
                        console.log(" result.text : ", result.text)
                        
                        this.organizationSearchService.findOrginazationByCode(result.text).subscribe(data=>{
                            if(data.body!=null){
                                 this.router.navigate(['',{ outlets: { paymentTab: ['payment', data.body.uID,data.body.tradingName] } }]);
                            }else{
                                dialogs.alert({
                                    title: "INVALID QR CODE ",
                                    message: "Format: " + result.format + ",\nContent: " + result.text,
                                    okButtonText: "OK"
                                });
                            }
                        },(error)=>{
                            dialogs.alert({
                                title: "something went wrong try again later",
                                message: "Format: " + result.format + ",\nContent: " + result.text,
                                okButtonText: "OK"
                            });
                        })
                        dialogs.alert({
            title: "You Scanned ",
            message: "Format: " + result.format + ",\nContent: " + result.text,
            okButtonText: "OK"
        });
        }, (errorMessage) => {
            console.log("Error when scanning " + errorMessage);
        }
    );
}

}
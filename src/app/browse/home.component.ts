// import { Component, OnInit } from "@angular/core";
// import { BrowserService } from './services/browser.service';
// import { DataService, IDataItem } from "../core/data.service";
// import { ObservableArray, ChangedData } from "tns-core-modules/data/observable-array";
// import { Router} from '@angular/router';


// @Component({
//     selector: "Browse",
//     moduleId: module.id,
//     templateUrl: "./browse.component.html",
//     styles:[`
//     .scrollTitle{
//         background-color:#000080;
//         text-align:center;
//         align:middle;
//         color:black;
//         padding-top:25px;
//         padding-left:15px;
//         padding-bottom:15px;
//         border-radius:50%;
//     }
//     .scroll{
//       padding-left:14px;  
//     }

//     `]
// })
// export class BrowseComponent implements OnInit {

//     recentOrganizationListElements:any[];

//     constructor(private router:Router,private browserService:BrowserService ) {
//     }

//     ngOnInit(): void {

//             this.browserService.getRecentOrganizationList().subscribe((data)=>{
//               this.browserService.getAllOrganizations(data.body).subscribe((res)=>{
//                 this.recentOrganizationListElements=res.body;
//               });
//             });

//     }

//     onListTap(event){
//         console.log(event);
//     }

// }

import { Component, OnInit } from '@angular/core';
import { Organization,ServiceDto } from '../common/models/models';
import { OrganizationSearchService } from '~/app/payment-search/services/organization-search.service';
import { TextField } from "tns-core-modules/ui/text-field";
import { Router } from '@angular/router';
import { BarcodeScanner } from 'nativescript-barcodescanner';
import { BrowserService } from './services/browser.service';
import * as dialogs from "tns-core-modules/ui/dialogs";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";

@Component({
    selector: "Browse",
    moduleId: module.id,
    templateUrl: "./home.component.html",
    styles: [`
    .scrollTitle{
        background-color:#000080;
        text-align:center;
        align:middle;
        color:black;
        padding-top:25px;
        padding-left:15px;
        padding-bottom:15px;
        border-radius:50%;
    }
    .scroll{
      padding-left:14px;  
      word-break: break-all;
    }
    .line{
        border-bottom:3px solid black;
    }
    .slider-image {
        
        }
        
        /** Slider title format */
        .slider-title {
            color: #fff;
            font-weight: bold;
            background-color: rgba(125, 125, 125);
            padding: 8;
            text-align: center;
            vertical-align: bottom;
        }
        
        /** Arrows */
        .arrow {
            color: #ffffff;
            font-size: 32;
            vertical-align: middle;
            padding: 10;
        }
        
        /** Arrow left wrapper */
        .arrow.arrow-left {
        
        }
        
        /** Arrow right */
        .arrow.arrow-right {
        
        }
    
  
    `]

})
export class BrowseComponent implements OnInit {
    organizations: Organization[];
    organization: Organization;
    serviceDto:ServiceDto[]
    name: string;
    recentOrganizationListElements:any[];
    public images:any[];
    recentTransactions:boolean=false;  
    noRecords:boolean=false;


    constructor(private browserService:BrowserService,private barcodeScanner: BarcodeScanner, private organizationSearchService: OrganizationSearchService, private router: Router) {

      
     }

    ngOnInit() {
        
        this.browserService.getRecentOrganizationList().subscribe((data)=>{
                          this.browserService.getAllOrganizations(data.body).subscribe((res)=>{
                            this.recentOrganizationListElements=res.body;
                            console.log("recentTransactions>>>>>>",res.body)
                            // if(data.body!=null){
                            //     this.recentTransactions=true;
                            //    this.noRecords=false;
                            // }
                            // this.recentTransactions=false;
                            // this.noRecords=true;
                          });
                        });
    }
  

    onTextChange(event: any) {
        let textField = <TextField>event.object;
        console.log("onTextChange");
        this.name = textField.text;
        console.log("name", this.name);
        if(this.name!=null && this.name.length<2){
            return;
        }
        this.organizationSearchService.getAllOrganizationList(this.name).subscribe((data) => {
            this.organizations = data.body;
            console.log("organizations:", data.body);
        })
    }
    
    onItemTap(event: any) {
        console.log("clicked");
    }


    onScan() {
        this.barcodeScanner.scan({
            formats: "QR_CODE, EAN_13",
            showFlipCameraButton: true,
            preferFrontCamera: false,
            showTorchButton: true,
            beepOnScan: true,
            torchOn: false,
            resultDisplayDuration: 500,
            openSettingsIfPermissionWasPreviouslyDenied: true
        }).then((result) => {
            console.log(" result.text : ", result.text)

            this.organizationSearchService.findOrginazationByCode(result.text).subscribe(data => {
               let org : Organization=data.body;
               console.log("Organization:",data.body)
                if (data.body != null) {
                    if(org.service[0].code=='PAY'){
                        this.router.navigate(['', { outlets: { paymentTab: ['payment', data.body.uID, data.body.tradingName] } }]);
                        console.log("payyyyy>>",  data.body.uID)
                    }else if(org.service[0].code=='BILLPAY'){
                        this.router.navigate(['',{outlets:{searchTab:['Biller',data.body.uID,data.body.tradingName]}}]);
                        console.log("BILLPAYYY>>",  data.body.uID)

                    }else if(org.service[0].code=='MER'){
                        this.router.navigate(['',{outlets:{browseTab:['paymentAmount',data.body.uID,data.body.tradingName]}}]);
                        console.log("MERCHANT>>",  data.body.uID)
                    }else{
                        dialogs.alert({
                            title: "MERCHANT NOT FOUND",
                            message: "Format: " + result.format + ",\nContent: " + result.text,
                            okButtonText: "OK"
                        });
                        
                    }
                } else {
                    dialogs.alert({
                        title: "INVALID QR CODE ",
                        message: "Format: " + result.format + ",\nContent: " + result.text,
                        okButtonText: "OK"
                    });
                }
            }, (error) => {
                dialogs.alert({
                    title: "something went wrong try again later",
                    message: "Format: " + result.format + ",\nContent: " + result.text,
                    okButtonText: "OK"
                });
            })
            dialogs.alert({
                title: "You Scanned ",
                message: "Format: " + result.format + ",\nContent: " + result.text,
                okButtonText: "OK"
            });
        }, (errorMessage) => {
            console.log("Error when scanning " + errorMessage);
        }
        );
    }
    myChangeEvent(args){
        var changeEventText = 'Page changed to index: ' + args.index;
        console.log(changeEventText);
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
}


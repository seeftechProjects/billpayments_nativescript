import { ActivatedRoute } from '@angular/router';
import { Component } from "@angular/core";

@Component({
    selector:"payment-amount",
    moduleId: module.id,
    templateUrl: "./payment-amount.component.html",
})
export class PaymentAmountComponent{
tradingName:string;
uid:string;
constructor(private activatedRoute:ActivatedRoute){}

    ngOnInit() {
         this.uid = this.activatedRoute.snapshot.params.uid;
        this.tradingName=this.activatedRoute.snapshot.params.tradingName;
       console.log("tradingName : ",this.tradingName)
    }
}
import { Injectable } from '@angular/core';
import { Http,Headers,RequestOptions,Response} from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators'
import { HttpClient, HttpHeaders, HttpResponse } from "@angular/common/http";
import {EntityResponse,PagedBackedResponse, Organization,PaymentsDTO,BackedResponse } from '../../common/models/models';
import { AppConstants } from '~/app/app-constants.service';

@Injectable()
export class BrowserService{
    constructor(private http:Http, private appConst : AppConstants){}


getRecentOrganizationList():Observable<EntityResponse<string[]>>{
    return this.http.get(this.appConst.API_BASE_URL+"/receipt/public/payments/recentOrganizations").pipe(map((response:Response)=>{
        return <EntityResponse<string[]>>response.json()
    }))
}


    getAllOrganizations(orgUidList : string[]):Observable<EntityResponse<Organization[]>>{
    return this.http.post(this.appConst.API_BASE_URL+"/oauth/public/organization/allOrginizations",orgUidList).pipe(map((response:Response)=>{
        return<EntityResponse<Organization[]>>response.json();
    }))
    }

    getPaymentList(pageNumber:number,pageSize:number):Observable<PagedBackedResponse<PaymentsDTO>>{
        return this.http.get(this.appConst.API_BASE_URL+"/receipt/public/payments/recentPayments/pageNumber/pageSize").pipe(map((response:Response)=>{
            return<PagedBackedResponse<PaymentsDTO>>response.json();
        }))
    }

}
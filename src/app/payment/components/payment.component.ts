import { PaymentTypeService } from '../services/payment.service';
import { Component, OnInit } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router';
import { CommonPaymentConfigDTO, PaymentCategoryDTO, PaymentConfigDTO, PaymentDataConfigDTO, CollectPaymentRequest, PaymentDataDTO } from '../../common/models/models';
import { SelectedIndexChangedEventData, ValueList } from "nativescript-drop-down";
import { TextField } from "ui/text-field";
import { RouterExtensions } from "nativescript-angular/router";

@Component({
    selector: "payment",
    moduleId: module.id,
    templateUrl: "./payment.component.html",
    styles:[`
    .list-group-item{
        display: flex;
        flex: 1 0 240px;
        align-items: center;
        font-size:20px;
        flex-flow: column nowrap;
    }
    .dropdown{
        padding: 7 5;
        border-bottom-style: solid;
        border-bottom-width: 1;
        border-bottom-color: #808080;

    }
    .item-drop-down{
        font-size: 15;
        height: 40;
        width: 100%;
        border: 2px solid;
        border-color:black;
    }
    .dropdownStack{
      margin:25px;
    }
    .gridHeaderClass{
        background-color:skyblue;
        height:30;
        padding-top:7px;
        padding-right:20px;
        text-align:center;
        margin-top:40px;
       
    }
    .hint{
        color:black;
    }
    `]

})
export class PaymentComponent implements OnInit {

    public test: PaymentCategoryDTO;
    public hint = "Please select";
    public items: ValueList<string>;
    public cssClass: string = "default";


    private orgCode: string;

    private commonPaymentConfigDTO: CommonPaymentConfigDTO = new CommonPaymentConfigDTO();
    private categoryChildren:  PaymentCategoryDTO[] = [];
    private selectedChildren: PaymentCategoryDTO[] = [];
    private showPaymentConfiguration: boolean = false;
    private paymentConfig: PaymentConfigDTO = new PaymentConfigDTO();
    private dataConfigsDTOs: PaymentDataConfigDTO[] = [];
    private selectedChild: PaymentCategoryDTO = new PaymentCategoryDTO();
    private imageSrc: string;
    private imagePath:string;
    buttonloading: boolean = false;
    pageLoading: boolean = false;
    paymentDto: PaymentDataDTO = new PaymentDataDTO;
    orgUid: string;
    lastCat : PaymentCategoryDTO = null;
    showOrgLogo:boolean=false;
    tradingName:string;

    constructor(private routerExtensions: RouterExtensions,private paymentTypeService: PaymentTypeService, private activatedRoute: ActivatedRoute, private router: Router) {
      //  this.orgCode = this.activatedRoute.snapshot.params['orgcode'];
    }

    ngOnInit() {
         const uid = this.activatedRoute.snapshot.params.uid;
         this.tradingName=this.activatedRoute.snapshot.params.tradingName;
        console.log("tradingName : ",this.tradingName)
        this.pageLoading = true;

            if(uid!=null){
                this.getPaymentCategory(uid);
            }
      
    }

    getPaymentCategory(uid:string){
        this.paymentTypeService.getPaymentCategories(uid).subscribe(paymentCategoryReposnse => {
            this.showOrgLogo=true;
              this.commonPaymentConfigDTO.paymentCategory = paymentCategoryReposnse.body.paymentCategory;
              this.commonPaymentConfigDTO.paymentPageConfigurationDTO = paymentCategoryReposnse.body.paymentPageConfigurationDTO;
              this.pageLoading = false;
              console.log("this.orgUid : ",uid);
             // // this.imagePath="/public/file/getfile/"
              //this.imageSrc = environment.apiUrl + "receipt/public/file/getfile/" + this.commonPaymentConfigDTO.paymentPageConfigurationDTO.orgLogoUid;
              if ((this.commonPaymentConfigDTO.paymentCategory.children == null && this.commonPaymentConfigDTO.paymentCategory.paymentConfig != null)
                  || (this.commonPaymentConfigDTO.paymentCategory.children == undefined && this.commonPaymentConfigDTO.paymentCategory.paymentConfig != undefined)) {
                  this.showPaymentConfiguration = true;
                  this.paymentConfig = this.commonPaymentConfigDTO.paymentCategory.paymentConfig;
                  if (this.paymentConfig.dataConfigsDTOs != null || this.paymentConfig.dataConfigsDTOs != undefined) {
                      this.dataConfigsDTOs = this.paymentConfig.dataConfigsDTOs;
                  }
              } else 
                  if (this.commonPaymentConfigDTO.paymentCategory.children != null || this.commonPaymentConfigDTO.paymentCategory.children != undefined) {
                      this.showPaymentConfiguration = false;
                      this.pushSelectedChildren(this.commonPaymentConfigDTO.paymentCategory.children);
                      this.populateValueList(this.commonPaymentConfigDTO.paymentCategory);
                      let paymentCategoryDTO: PaymentCategoryDTO = new PaymentCategoryDTO();
                      paymentCategoryDTO = this.commonPaymentConfigDTO.paymentCategory;
                      this.categoryChildren.push(paymentCategoryDTO);

                      // const valueList: ValueList<PaymentCategoryDTO[]>  = new ValueList<PaymentCategoryDTO[]>();
                      // valueList.push({value:this.commonPaymentConfigDTO.paymentCategory.children, display: ""})
                      
                      // this.categoryChildren.push({valueList});
                  }
          }, (error) => {
              this.pageLoading = false;
          });
    }

    populateValueList(paymentCategory : PaymentCategoryDTO){
        if(paymentCategory.children!=null){
        paymentCategory.valueList=new ValueList<PaymentCategoryDTO>();
        paymentCategory.children.forEach(e=>{
            paymentCategory.valueList.push({value:e,display:e.name})
            this.populateValueList(e);
        })
        }

    }

    loadChildren(event,parent, index) {
      this.showOrgLogo=true;
        this.showPaymentConfiguration = false;
        this.selectedChild = new PaymentCategoryDTO();
        this.selectedChildren.forEach(selectedObj => {
            if (selectedObj.id == parent.children[event.newIndex].id) {
                this.selectedChild = selectedObj;
            }
        })

        if (this.categoryChildren.length > index) {
            for (let i = index; i < this.categoryChildren.length; i++) {
                this.categoryChildren.splice(i + 1, 1);
            }
        }

        if ((this.selectedChild.children == null && this.selectedChild.paymentConfig != null) || (this.selectedChild.children == undefined && this.selectedChild.paymentConfig != undefined)) {
            this.showPaymentConfiguration = true;
            this.lastCat=this.selectedChild;
            this.paymentConfig = this.selectedChild.paymentConfig;
            console.log("paymentConfig", this.paymentConfig)
            if (this.paymentConfig.dataConfigsDTOs != null || this.paymentConfig.dataConfigsDTOs != undefined) {
                this.dataConfigsDTOs = this.paymentConfig.dataConfigsDTOs;
            }
        } else
            if (this.selectedChild.children != null || this.selectedChild.children != undefined) {
                this.showPaymentConfiguration = false;
                const paymentCategoryDTO: PaymentCategoryDTO = new PaymentCategoryDTO();
                paymentCategoryDTO.name = this.selectedChild.name;
                paymentCategoryDTO.children = this.selectedChild.children;
                this.populateValueList(paymentCategoryDTO);
                this.categoryChildren.push(paymentCategoryDTO);
            }
    }

    pushSelectedChildren(dtos:PaymentCategoryDTO[]) {
      this.showOrgLogo=true;
        dtos.forEach(child => {
            this.selectedChildren.push(child);

            if (child.children != null || child.children != undefined) {
                this.pushSelectedChildren(child.children );
             }
        });
    }

    savePayment() {
        this.buttonloading = true;
        this.showOrgLogo=true;
        let collectPaymentRequest = new CollectPaymentRequest();
        collectPaymentRequest.amount = this.paymentConfig.amount;
        console.log("Amount:", this.paymentConfig.amount)
        collectPaymentRequest.productUid = this.selectedChild.uid;
        collectPaymentRequest.additionalInfo = [];
        this.dataConfigsDTOs.forEach(e => {
            collectPaymentRequest.additionalInfo[e.name] = e.data;
        })
        this.paymentTypeService.savePayment(collectPaymentRequest, this.orgUid).subscribe((data) => {
            this.buttonloading = false;
            // this.collectPaymentRequest = data.body;
        }, (error) => {
            this.buttonloading = false;
        })
    }

    initiatePayment() {
        this.buttonloading = true;
        this.showOrgLogo=true;
        let collectPaymentRequest = new CollectPaymentRequest();
        collectPaymentRequest.amount = this.paymentConfig.amount;
        collectPaymentRequest.productUid = this.selectedChild.uid;
        collectPaymentRequest.currency = "XOF";
        collectPaymentRequest.appCode = "SMRECP";
        collectPaymentRequest.description = this.parepareDescription();
        collectPaymentRequest.additionalInfo = {};
        console.log(this.dataConfigsDTOs);
        this.dataConfigsDTOs.forEach(e => {
            collectPaymentRequest.additionalInfo[e.name] = e.data;
        })
     
        this.paymentTypeService.initiatePayment(collectPaymentRequest, this.orgUid).subscribe((data) => {
            this.processPaymentResponse(data.body);
        }, (error) => {
            this.buttonloading = false;
        })
    }

    private parepareDescription(): string {
      this.showOrgLogo=true;
        let description:string="";
        for(let i=1;i< this.categoryChildren.length;i++){
            description=description+this.categoryChildren[i].name+" -> ";
        }
        description=description + this.lastCat.name;
        return description;
    }

    public processPaymentResponse(paymentResponse: any) {
      this.showOrgLogo=true;
       
        if (paymentResponse.status.toLowerCase() == 'initiated') {
            window.location.href = paymentResponse.paymentURL;
        } else {
            this.buttonloading = false;

        }
    }
    public goBack() {
        this.routerExtensions.backToPreviousPage();
    }
}

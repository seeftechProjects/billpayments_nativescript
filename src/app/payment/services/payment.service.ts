import { Injectable } from "@angular/core";
import { Http, RequestOptions, Headers, Response } from "@angular/http";
import { BackedResponse, EntityResponse, PaymentCategoryDTO, CommonPaymentConfigDTO, CollectPaymentRequest, PaymentDetails } from "../../common/models/models";
import { Observable } from "rxjs";
import { map } from 'rxjs/operators'
import { HttpClient, HttpHeaders, HttpResponse } from "@angular/common/http";
import { AppConstants } from "~/app/app-constants.service";
@Injectable()
export class PaymentTypeService{

    constructor(private http:Http, private appConst : AppConstants){
    }

    public getOrganizationCode(orgCode:string):Observable<EntityResponse<string>>{
        return this.http.get("http:/192.168.1.22:8084/oauth/public/organization/getuid/"+orgCode).pipe(map((response:Response)=>{
            let orgUidResponse : EntityResponse<string> = <EntityResponse<string>>response.json();
            return orgUidResponse;
        }))
    }

    public getPaymentCategories(orgUid : string):Observable<EntityResponse<CommonPaymentConfigDTO>>{
        console.log(orgUid);
        let headers=new Headers({ 'Content-Type': 'application/json', 'X-ORG-ID' : orgUid});
        let options =new RequestOptions({ headers: headers} );
        return this.http.get(this.appConst.API_BASE_URL+"receipt/common/getPaymentConfig",options).pipe(map((response :Response)=>{
          console.log(orgUid);
            return response.json();
        }))
    }

    public savePayment(collectPaymentRequest:CollectPaymentRequest,orgUid:string):Observable<EntityResponse<CollectPaymentRequest>>{
        let headers=new Headers({ 'Content-Type': 'application/json', 'X-ORG-ID' : orgUid});
        let options =new RequestOptions({ headers: headers} );
        return this.http.post(this.appConst.API_BASE_URL+"/receipt/public/payments/collect",collectPaymentRequest,options).pipe(map((response:Response)=>{
            return<EntityResponse<CollectPaymentRequest>>response.json()
        }))
    }

    public initiatePayment(collectPaymentRequest:CollectPaymentRequest,orgUid:string):Observable<EntityResponse<CollectPaymentRequest>>{
        let headers=new Headers({ 'Content-Type': 'application/json', 'X-ORG-ID' : orgUid});
        let options =new RequestOptions({ headers: headers} );
        return this.http.post(this.appConst.API_BASE_URL+"/payments/public/gateway/epay/initiate",collectPaymentRequest,options).pipe(map((response:Response)=>{
            return<EntityResponse<CollectPaymentRequest>>response.json()
        }))
    }

    public getPaymentDetails(refNum : string):Observable<EntityResponse<PaymentDetails>>{
        return this.http.get(this.appConst.API_BASE_URL+"/payments/public/payment/getpaymentdetails?"+refNum).pipe(map((response:Response)=>{
            return<EntityResponse<PaymentDetails>>response.json()
        }))
    }
    
}

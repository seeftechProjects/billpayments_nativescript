import { Component, OnInit } from "@angular/core";
import { DropDownModule } from "nativescript-drop-down/angular";
import { OrganizationSearchService } from '../payment-search/services/organization-search.service';
import { Router,ActivatedRoute } from '@angular/router';
import {Organization} from '../common/models/models'
import { TextField } from "tns-core-modules/ui/text-field/text-field";
import { RouterExtensions } from "nativescript-angular/router";



@Component({
    selector: "Search",
    moduleId: module.id,
    templateUrl: "./search.component.html"
    // styles:[`
    // .gridClass{
    // margin:20px;
    // font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    // border-collapse: collapse;
    // width: 100%;
    // }
    // .stackClass{
    //     margin-left:30px;
    //     border:1px solid black;
    // }   
    // .gridHeaderClass{
    //     background-color:skyblue;
    //     height:30;
    //     padding-top:5px;
    //     padding-right:20px;
    //     text-align:center;
    // }
    // .header{
    //     margin-right:7px;
    //     color:black;
    // }
    // .stackClass{
    //     margin:20px;
    // }
    
})
export class SearchComponent implements OnInit {
    organizations:Organization[];
    organization:Organization;
    name:string;
    tradingName:string;

    constructor(private routerExtensions: RouterExtensions,private organizationSearchService:OrganizationSearchService,private router:Router,private activatedRoute:ActivatedRoute) {
   
    }

  
    public ngOnInit() {
        const uid = this.activatedRoute.snapshot.params.uid;
        this.tradingName=this.activatedRoute.snapshot.params.tradingName;
        
    }

    onTestChange(event:any){
          let textField = <TextField>event.object;
                  console.log("onTextChange");
                  this.name = textField.text;
                  console.log("name",this.name);
            this.organizationSearchService.getAllOrganizationList(this.name).subscribe((data)=>{
                this.organizations=data.body;
                console.log("organizations:",data.body);
            })
        }
        onItemTap(event:any){
            console.log("clicked");
        }


        public goBack() {
            this.routerExtensions.backToPreviousPage();
        }
}
